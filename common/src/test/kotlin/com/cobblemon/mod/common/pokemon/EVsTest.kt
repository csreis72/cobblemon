package com.cobblemon.mod.common.pokemon
import com.cobblemon.mod.common.api.pokemon.stats.Stats
import com.cobblemon.mod.common.junit.BootstrapMinecraft
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

@BootstrapMinecraft
internal class EVsTest {

    private lateinit var evs: EVs

    @BeforeEach
    fun setUp() {
        evs = EVs.createEmpty()
    }

    @Test
    fun `test add when total EVs is at maximum and adding positive value`() {
        // CT1
        evs[Stats.HP] = EVs.MAX_STAT_VALUE
        evs[Stats.ATTACK] = EVs.MAX_STAT_VALUE
        evs[Stats.DEFENCE] = 6

        val result = evs.add(Stats.DEFENCE, 10)
        assertEquals(0, result)
    }

    @Test
    fun `test add when total EVs is at maximum and adding negative value`() {
        // CT2
        evs[Stats.HP] = EVs.MAX_STAT_VALUE
        evs[Stats.ATTACK] = EVs.MAX_STAT_VALUE
        evs[Stats.DEFENCE] = 6

        val result = evs.add(Stats.DEFENCE, -7)
        assertEquals(-6, result)
    }

    @Test
    fun `test add when current stat is at max and adding positive value`() {
        // CT3
        evs[Stats.HP] = EVs.MAX_STAT_VALUE
        val result = evs.add(Stats.HP, 10)
        assertEquals(0, result)
    }
}